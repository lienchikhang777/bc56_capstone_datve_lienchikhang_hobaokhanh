import React from "react";
import { useSelector } from "react-redux";
import { SyncLoader } from "react-spinners";

export default function Spinner() {
  const isFetching = useSelector((state) => state.LoadingSlice.isFetching);

  return (
    <div>
      {isFetching && (
        <div className="h-screen w-screen fixed top-0 bottom-0 left-0 right-0 bg-black z-10 flex justify-center items-center">
          <SyncLoader
            color="orange"
            loading={true}
            // cssOverride={override}
            size={100}
            aria-label="Loading Spinner"
            data-testid="loader"
          />
        </div>
      )}
    </div>
  );
}
