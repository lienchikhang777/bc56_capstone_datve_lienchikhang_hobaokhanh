import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { phimService } from "../../services";
import { Rate, Tabs } from "antd";

export default function FilmDetail() {
  const { id } = useParams();
  const [film, setFilm] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    phimService
      .getDetailFilm(id)
      .then((res) => [setFilm(res.data)])
      .catch((err) => console.log(err));
  }, []);

  const handleSelectTime = (maLichChieu) => {
    navigate(`/purchase/${maLichChieu}`);
  };

  const renderList = () => {
    return film?.heThongRapChieu.map((heThongRap) => {
      return {
        key: heThongRap.maHeThongRap,
        label: <img src={heThongRap.logo} width={50} />,
        children: (
          <div>
            <div className="top">
              {heThongRap?.cumRapChieu.map((cumRap) => {
                return (
                  <div>
                    <h1>{cumRap.tenCumRap}</h1>
                    <ul className="list-none flex">
                      {cumRap.lichChieuPhim.map((lichChieu) => {
                        let day = lichChieu.ngayChieuGioChieu.split("T")[0];
                        let time = lichChieu.ngayChieuGioChieu.split("T")[1];
                        return (
                          <li
                            className="p-4 mx-2"
                            onClick={() =>
                              handleSelectTime(lichChieu.maLichChieu)
                            }
                          >
                            <p className="datetime text-white">
                              <span className="text-green-500">{day}</span> ~
                              <span className="text-orange-500">{time}</span>
                            </p>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                );
              })}
            </div>
          </div>
        ),
      };
    });
  };

  const renderingUI = () => {
    return (
      <div className="container">
        <div className="detail__wrapper relative">
          <div className="detail__banner-wrapper absolute">
            <img src={film?.hinhAnh} className="detail__banner" />
          </div>
          <div className="grid grid-cols-12 gap-3 p-8">
            <div className="col-span-3">
              <img
                src={film?.hinhAnh}
                width={300}
                height={200}
                className="detail__img"
              />
            </div>
            <div className="col-span-9 flex justify-center items-start flex-col">
              <div className="flex mb-3">
                <div className="detail__info-wrapper mr-4 text-white">
                  <div className="detail__info">
                    <p className="detail__time">
                      {film?.ngayKhoiChieu.split("T")[0]}
                    </p>
                    <p className="detail__title text-4xl font-medium">
                      {film?.tenPhim}
                    </p>
                  </div>
                </div>
                <div className="detail__info-wrapper text-white">
                  <div className="detail__rate">
                    <p className="detail__time font-medium mb-4">Đánh giá</p>
                    <Rate />
                    {/**công thức suy ra 5 sao: số sao =  (n điểm  * 5) / 10 */}
                  </div>
                </div>
              </div>
              <div className="detail__info-wrapper">
                <a className="cta-btn" href="#tabs">
                  MUA NGAY
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="p-24 picker__wrapper my-4" id="tabs">
          <Tabs defaultActiveKey="1" items={renderList()} tabPosition="left" />
        </div>
      </div>
    );
  };

  return <div>{renderingUI()}</div>;
}
