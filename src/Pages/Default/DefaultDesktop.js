import React from "react";
import CarouselDatVe from "../../components/Carousel/CarouselDatVe";
import DanhSachPhim from "./DanhSachPhim/DanhSachPhim";
import TabPhim from "./TabPhim/TabPhim";

export default function DefaultDesktop() {
  return (
    <>
      <CarouselDatVe />
      <DanhSachPhim />
      <TabPhim />
      
    </>
  );
}
