import { Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { rapService } from "../../../services";
import { useHref } from "react-router-dom";

export default function TabPhim() {
  const [rapList, setRapList] = useState(null);
  const [listCumRap, setListCumRap] = useState(null);

  useEffect(() => {
    Promise.all([
      rapService.getThongTinHeThongRap(),
      rapService.getThongTinLichChieuHeThongRap("BHDStar"),
    ])
      .then((res) => [
        console.log(res),
        setRapList(res[0].data),
        setListCumRap(res[1].data[0]),
      ])
      .catch((err) => console.log(err));
  }, []);

  const convertStr = (str) => {
    if (str.length > 30) return str.slice(0, 30) + "...";
    return str;
  };

  const renderingUI = () => {
    return (
      rapList &&
      rapList.map(({ logo, maHeThongRap }, index) => {
        return {
          key: maHeThongRap,
          label: <img src={logo} width={50} />,
          children: (
            <Tabs
              defaultActiveKey="1"
              items={renderingDanhSachCumRap()}
              tabPosition="left"
              // onChange={onChangeCumRap}
            />
          ),
        };
      })
    );
  };

  const renderingDanhSachCumRap = () => {
    return (
      listCumRap &&
      listCumRap.lstCumRap.slice(0, 10).map((cumRap) => {
        return {
          key: cumRap.maCumRap,
          label: (
            <div className="text-start">
              <h1 className="text-sm text-green-500 font-bold">
                {convertStr(cumRap.tenCumRap)}
              </h1>
              <p className="inline-block text-sm text-gray-400">
                {convertStr(cumRap.diaChi)}
              </p>
              <p className="text-orange-500 font-bold">Chi tiết</p>
            </div>
          ),
          children: (
            <div>
              {cumRap.danhSachPhim.slice(0, 10).map((phim) => {
                return (
                  <div className="grid grid-cols-7 gap-6 p-4 h-56">
                    <div className="col-span-2">
                      <img src={phim.hinhAnh} width={100} className="h-full" />
                    </div>
                    <div className="col-span-5">
                      <div className="flex items-center mb-2">
                        <span className="mr-4 bg-red-500 rounded-sm text-white font-bold p-2">
                          C18
                        </span>
                        <h1 className="text-xl font-bold">{phim.tenPhim}</h1>
                      </div>
                      <div className="grid grid-cols-2">
                        {phim.lstLichChieuTheoPhim
                          .slice(0, 4)
                          .map((lichChieu) => {
                            let day = lichChieu.ngayChieuGioChieu.split("T")[0];
                            let time =
                              lichChieu.ngayChieuGioChieu.split("T")[1];
                            return (
                              <div className="dayTime__wrapper rounded-sm p-2 bg-gray-100 m-1">
                                <p className="text-sm font-bold">
                                  <span className="text-green-500">{day}</span>
                                  <br></br>
                                  <span className="text-orange-500">
                                    {time}
                                  </span>
                                </p>
                              </div>
                            );
                          })}
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          ),
        };
      })
    );
  };

  const onChange = async (key) => {
    try {
      const data = await rapService.getThongTinLichChieuHeThongRap(key);
      setListCumRap(data.data[0]);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="py-12 px-44 container">
      <Tabs
        defaultActiveKey="1"
        items={renderingUI()}
        tabPosition="left"
        onChange={onChange}
      />
    </div>
  );
}
