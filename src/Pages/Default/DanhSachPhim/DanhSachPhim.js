import React from "react";
import { useSelector } from "react-redux";
import FilmItem from "./FilmItem";
import { Carousel } from "antd";

export default function DanhSachPhim() {
  const filmList = useSelector((state) => state.filmSlice.filmList);
  const carouselFilmRef = React.createRef();

  const pagination = () => {
    let itemPerCarousel = 8;
    let sliceFilmList = [];
    for (let i = 0; i < filmList?.length; i += itemPerCarousel) {
      sliceFilmList.push(filmList.slice(i, i + itemPerCarousel));
    }
    return sliceFilmList;
  };

  const renderingUI = () => {
    let filmList = [...pagination()];
    return filmList.map((filmArr) => {
      return (
        <div className="relative carouselFilm__wrapper grid grid-cols-4 gap-5">
          {filmArr.map((film) => {
            return <FilmItem film={film} />;
          })}
        </div>
      );
    });
  };

  const onChange = (currentSlide) => {};

  return (
    <div className="py-24 px-56 container">
      <Carousel
        afterChange={onChange}
        ref={carouselFilmRef}
        className="my-test"
      >
        {renderingUI()}
      </Carousel>
    </div>
  );
}
