import React from "react";
import { useNavigate } from "react-router-dom";

export default function FilmItem({ film }) {
  console.log("fil,", film);
  const { maPhim, tenPhim, hinhAnh, moTa } = film;
  const navigate = useNavigate();

  const convertStr = (str) => {
    if (str.length > 50) return str.slice(0, 50) + "...";
    return str;
  };

  const convertTitleStr = (str) => {
    if (str.length > 12) return str.slice(0, 12) + "...";
    return str;
  };

  const handleClick = (idFilm) => {
    navigate(`detail/${idFilm}`);
  };

  return (
    <div className="filmItem__wrapper relative">
      <img src={hinhAnh} width={200} className="w-full h-80 rounded-md" />
      <div className="sub__wrapper">
        <div className="flex mt-3">
          <span className="mr-2 bg-red-600 text-white text-sm font-bold p-1 rounded-sm">
            C18
          </span>
          <h2 className="my-text font-medium">{convertTitleStr(tenPhim)}</h2>
        </div>
        <p className="text-sm font-medium text-stone-500 mt-3">
          {convertStr(moTa)}
        </p>
      </div>
      <button
        className="hover-btn absolute bottom-10 left-0 my-btn full-width"
        onClick={() => handleClick(maPhim)}
      >
        MUA NGAY
      </button>
    </div>
  );
}
