import React, { useEffect, useState } from "react";
import { phongVeService, veService } from "../../services";
import { useParams } from "react-router-dom";
import { Modal } from "antd";

export default function Purchase() {
  const { maLichChieu } = useParams();
  const [phongVe, setPhongVe] = useState(null);
  const [chooseSeat, setChooseSeat] = useState([]);

  console.log("choose", chooseSeat);

  useEffect(() => {
    phongVeService
      .getDanhSachPhoneVe(maLichChieu)
      .then((res) => [console.log(res), setPhongVe(res.data)])
      .catch((err) => console.log(err));
  }, []);

  const convertStr = (str) => {
    if (str?.length > 30) return str.slice(0, 30) + "...";
    return str;
  };

  const handleChooseSeat = (newSeat) => {
    if (newSeat?.daDat) {
      return;
    }
    setChooseSeat([...chooseSeat, newSeat]);
  };

  const calTotal = () => {
    return chooseSeat.reduce((acc, curVar) => {
      return acc + curVar.giaVe;
    }, 0);
  };

  const handleDatVe = (maLichChieu) => {
    const user = localStorage.getItem("user");
    if (!user) {
      Modal.warning({
        title: "This is a warning message",
        content: "Vui lòng đăng nhập trước khi đặt vé",
      });
      return;
    }
    veService
      .datVe({
        maLichChieu: maLichChieu,
        danhSachVe: chooseSeat,
        taiKhoanNguoiDung: JSON.parse(localStorage.getItem("user"))?.taiKhoan,
      })
      .then((res) => {
        Modal.success({
          title: "This is a success message",
          content: "Đặt vé thành công",
        });
        phongVeService
          .getDanhSachPhoneVe(maLichChieu)
          .then((res) => [
            console.log(res),
            setPhongVe(res.data),
            setChooseSeat([]),
          ])
          .catch((err) => console.log(err));
      });
  };

  const renderingUI = () => {
    return (
      <div className="grid grid-cols-12">
        {phongVe?.danhSachGhe.map((seat) => {
          return (
            <div
              className={`my-seat ${
                seat.daDat ? "unactive" : seat.loaiGhe == "Thuong" ? "" : "vip"
              }`}
              onClick={() => handleChooseSeat(seat)}
            >
              {seat.tenGhe}
            </div>
          );
        })}
      </div>
    );
  };

  return (
    <div className="grid grid-cols-12 p-9">
      <div className="col-span-8 p-7">
        <div className="flex justify-center mb-4">
          <div className="flex items-center mx-2">
            <div className="block w-5 h-5 my-seat unactive"></div>
            <span>Đã đặt</span>
          </div>
          <div className="flex items-center mx-2">
            <div className="block w-5 h-5 my-seat"></div>
            <span>Thường</span>
          </div>
          <div className="flex items-center mx-2">
            <div className="block w-5 h-5 my-seat vip"></div>
            <span>Vip</span>
          </div>
        </div>
        {renderingUI()}
      </div>
      <div className="col-span-4">
        <div className="my-checkInfo">
          <h1 className="my-checkInfo__price text-green-700 text-5xl text-center">
            {calTotal().toLocaleString()} VND
          </h1>
          <ul className="my-ul-checkInfo">
            <li className="my-li-checkInfo">
              <span>Cụm rạp:</span>
              <span className="text-green-700">
                {phongVe?.thongTinPhim.tenCumRap}
              </span>
            </li>
            <li className="my-li-checkInfo">
              <span>Địa chỉ:</span>
              <span className="text-green-700">
                {convertStr(phongVe?.thongTinPhim.diaChi)}
              </span>
            </li>
            <li className="my-li-checkInfo">
              <span>Rạp:</span>
              <span className="text-green-700">
                {phongVe?.thongTinPhim.tenRap}
              </span>
            </li>
            <li className="my-li-checkInfo">
              <span>Ngày giờ chiếu:</span>
              <div className="my-div-checkInfo">
                <span className="text-green-700">
                  {phongVe?.thongTinPhim.ngayChieu}
                </span>
                ~
                <span className="text-orange-500">
                  {phongVe?.thongTinPhim.gioChieu}
                </span>
              </div>
            </li>
            <li className="my-li-checkInfo">
              <span>Tên Phim:</span>
              <span className="text-green-700">
                {phongVe?.thongTinPhim.tenPhim}
              </span>
            </li>
            <li className="my-li-checkInfo">
              <span>Chọn:</span>
              <div>
                {chooseSeat.map((seat) => (
                  <span className="text-xs">Ghế {seat.tenGhe}</span>
                ))}
              </div>
            </li>
          </ul>
          <button
            className="cta-btn checkInfo"
            onClick={() => handleDatVe(maLichChieu)}
          >
            ĐẶT VÉ
          </button>
        </div>
      </div>
    </div>
  );
}
