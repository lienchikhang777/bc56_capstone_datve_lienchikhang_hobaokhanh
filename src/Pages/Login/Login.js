import { Button, Form, Input, Modal } from "antd";
import React from "react";
import { userService } from "../../services";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { login } from "../../ReduxToolkit/Slices/userSlice";

export default function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onFinish = (values) => {
    console.log("Success:", values);
    userService
      .dangNhap(values)
      .then((res) => [
        console.log("user", res),
        localStorage.setItem("user", JSON.stringify(res.data)),
        dispatch(login(res.data)),
        Modal.success({
          title: "This is a success message",
          content: "Đăng nhập thành công",
        }),
        navigate("/"),
      ])
      .catch((err) => console.log(err));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div>
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 600,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Password"
          name="matKhau"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
