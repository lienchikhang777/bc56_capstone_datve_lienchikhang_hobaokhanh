import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import UserLayout from "./layouts/UserLayout";
import Default from "./Pages/Default/Default";
import FilmDetail from "./Pages/FilmDetail/FilmDetail";
import Purchase from "./Pages/Purchase/Purchase";
import Login from "./Pages/Login/Login";
import Spinner from "./Pages/spinner/Spinner";

function App() {
  return (
    <BrowserRouter>
      <Spinner />
      <Routes>
        <Route path="" element={<UserLayout />}>
          <Route path="" element={<Default />} />
          <Route path="/login" element={<Login />} />
          <Route path="/detail/:id" element={<FilmDetail />} />
          <Route path="/purchase/:maLichChieu" element={<Purchase />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
