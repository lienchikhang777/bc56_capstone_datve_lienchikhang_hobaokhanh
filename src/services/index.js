import { http } from "./config";

export const rapService = {
  getDanhSachRapTheoPhim: (idFilm) => {
    return http.get(`/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${idFilm}`);
  },
  getThongTinHeThongRap: () => {
    return http.get("/QuanLyRap/LayThongTinHeThongRap");
  },
  getThongTinLichChieuHeThongRap: (maRap) => {
    return http.get(
      `/QuanLyRap/LayThongTinLichChieuHeThongRap?maHeThongRap=${maRap}&maNhom=GP09`
    );
  },
};

export const phimService = {
  getDanhSachPhim: () => {
    return http.get("/QuanLyPhim/LayDanhSachPhim?maNhom=GP09");
  },
  getDetailFilm: (idFilm) => {
    return http.get(`/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${idFilm}`);
  },
};

export const phongVeService = {
  getDanhSachPhoneVe: (maLichChieu) => {
    return http.get(
      `/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`
    );
  },
};

export const veService = {
  datVe: (data) => {
    return http.post(`/QuanLyDatVe/DatVe`, data);
  },
};

export const userService = {
  dangNhap: (data) => {
    return http.post(
      `https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap`,
      data
    );
  },
};
