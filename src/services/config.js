import axios from "axios";
import { getAccessToken } from "./getAccessToken";
import store from "../ReduxToolkit/store";
import {
  turnOffLoading,
  turnOnLoading,
} from "../ReduxToolkit/Slices/LoadingSlice";

export const http = axios.create({
  baseURL: "https://movie0706.cybersoft.edu.vn/api",
  headers: {
    Authorization: `Bearer ${getAccessToken()}`,
    TokenCybersoft:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA1NiIsIkhldEhhblN0cmluZyI6IjE4LzA0LzIwMjQiLCJIZXRIYW5UaW1lIjoiMTcxMzM5ODQwMDAwMCIsIm5iZiI6MTY4MzMwNjAwMCwiZXhwIjoxNzEzNTQ2MDAwfQ.4A7jJib1YUkmnIr-QDcjs_3j1YY0Ft1wPZDfe8qFrqE",
  },
});

http.interceptors.request.use(
  function (config) {
    store.dispatch(turnOnLoading(true));
    return config;
  },
  function (err) {
    store.dispatch(turnOffLoading(false));
    return Promise.reject(err);
  }
);

http.interceptors.response.use(
  (response) => {
    store.dispatch(turnOffLoading(false));
    return response;
  },
  (err) => {
    store.dispatch(turnOffLoading(false));
    return Promise.reject(err);
  }
);
