import { configureStore } from "@reduxjs/toolkit";
import filmSlice from "./Slices/filmSlice";
import LoadingSlice from "./Slices/LoadingSlice";
import userSlice from "./Slices/userSlice";

const store = configureStore({
  reducer: { filmSlice, LoadingSlice, userSlice },
});

export default store;
