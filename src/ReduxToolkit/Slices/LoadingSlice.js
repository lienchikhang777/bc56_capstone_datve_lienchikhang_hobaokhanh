import { createSlice } from "@reduxjs/toolkit";

const loadingSlice = createSlice({
  name: "loading",
  initialState: {
    isFetching: false,
  },
  reducers: {
    turnOnLoading: (state, action) => {
      state.isFetching = action.payload;
    },
    turnOffLoading: (state, action) => {
      state.isFetching = action.payload;
    },
  },
});

export const { turnOffLoading, turnOnLoading } = loadingSlice.actions;
export default loadingSlice.reducer;
