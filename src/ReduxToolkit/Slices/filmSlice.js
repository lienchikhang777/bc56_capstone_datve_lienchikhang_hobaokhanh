import { createSlice } from "@reduxjs/toolkit";

const filmSlice = createSlice({
  name: "filmSlice",
  initialState: {
    filmList: null,
  },
  reducers: {
    setFilmList: (state, action) => {
      state.filmList = action.payload;
    },
  },
});
export const { setFilmList } = filmSlice.actions;
export default filmSlice.reducer;
