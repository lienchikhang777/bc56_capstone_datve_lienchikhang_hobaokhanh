import { Button, Collapse, Dropdown } from 'antd'
import React from 'react'

const items = [
    {
        key: '1',
        label: (
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                1st menu item
            </a>
        ),
    },
    {
        key: '2',
        label: (
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                2nd menu item
            </a>
        ),
    },
    {
        key: '3',
        label: (
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
                3rd menu item
            </a>
        ),
    },
];

export default function HeaderMobile() {
    return (
        <div className='header__wrapper container py-4'>
            <div className='header__logo'>
                <h2 className='text-xl'>Cyber Movie</h2>
            </div>
            {/* <div className="header__auth">
                <ul className='list-none flex justify-between items-center'>
                    <li className='header__navItem transition-all hover:after:w-full p-4'>
                        <a href="" className='font-medium'>LOGIN</a>
                    </li>
                    <li className='header__navItem transition-all hover:after:w-full p-4'>
                        <a href="" className='font-medium'>SIGN UP</a>
                    </li>
                </ul>
            </div> */}
            <div className="header__menu">
                <Dropdown
                    className='w-full'
                    menu={{
                        items,
                    }}
                    placement="bottomLeft">
                    <Button>|||</Button>
                </Dropdown>
            </div>
        </div>
    )
}
