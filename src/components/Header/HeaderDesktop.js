import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

export default function HeaderDesktop() {
  const navigate = useNavigate();
  const user = useSelector((state) => state.userSlice.user);
  const handleLogin = () => {
    navigate("/login");
  };

  const handleClickLogo = () => {
    navigate("/");
  };
  return (
    <div className="header__wrapper container">
      <div className="header__logo" onClick={handleClickLogo}>
        <h2 className="text-3xl text-orange-400">Cyber Movie</h2>
      </div>
      <div className="header__nav">
        <ul className="list-none flex justify-between items-center ">
          <li className="header__navItem transition-all hover:after:w-full p-5">
            <a href="" className="font-medium">
              Lịch chiếu
            </a>
          </li>
          <li className="header__navItem transition-all hover:after:w-full p-5">
            <a href="" className="font-medium">
              Cụm rạp
            </a>
          </li>
          <li className="header__navItem transition-all hover:after:w-full p-5">
            <a href="" className="font-medium">
              Tin tức
            </a>
          </li>
          <li className="header__navItem transition-all hover:after:w-full p-5">
            <a href="" className="font-medium">
              Ứng dụng
            </a>
          </li>
        </ul>
      </div>
      <div className="header__auth">
        <ul className="list-none flex justify-between items-center">
          <li
            className="header__navItem transition-all hover:after:w-full p-5"
            onClick={handleLogin}
          >
            {user ? (
              <div>{user.taiKhoan}</div>
            ) : (
              <a href="" className="font-medium">
                <i class="fa-solid fa-user"></i> Đăng nhập
              </a>
            )}
          </li>
          <li className="header__navItem transition-all hover:after:w-full p-5">
            <a href="" className="font-medium">
              <i class="fa-solid fa-user"></i> Đăng ký
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}
