import { Carousel, Modal, Select } from "antd";
import React, { useEffect, useState } from "react";
import { Player } from "video-react";
import { phimService, rapService } from "../../services";
import { useDispatch } from "react-redux";
import { setFilmList } from "../../ReduxToolkit/Slices/filmSlice";

export default function CarouselDatVe() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const carouselRef = React.createRef();
  const [curVid, setCurVid] = useState(0);
  const [filmSelectList, setFilmSelectList] = useState(null);
  const [movTheaterList, setMovThearterList] = useState(null);
  const [lichChieu, setLichChieu] = useState([]);
  const dispatch = useDispatch();
  const rapList = [];

  const source = {
    0: "https://www.youtube.com/embed/kBY2k3G6LsM",
    1: "https://www.youtube.com/embed/uqJ9u7GSaYM",
    2: "https://www.youtube.com/embed/JNZv1SgHv68",
  };

  const contentStyle = {
    margin: 0,
    height: "600px",
    width: "100vw",
  };

  const onChange = (currentSlide) => {
    setCurVid(currentSlide);
    console.log(currentSlide);
  };

  const handleChangeCarousel = (isNext) => {
    if (isNext) carouselRef.current.next();
    else carouselRef.current.prev();
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const handleChangePhim = (value) => {
    rapService
      .getDanhSachRapTheoPhim(value)
      .then((res) => setMovThearterList(res.data.heThongRapChieu))
      .catch((err) => console.log("err", err));
  };

  const handleChangeRap = (value) => {
    let lichChieuList = getLichChieuPhim(value);
    let arrLichChieu = lichChieuList.map((lichChieu) => {
      return {
        label: lichChieu.ngayChieuGioChieu,
        value: lichChieu.maLichChieu,
      };
    });
    arrLichChieu.unshift({
      label: "Ngày giờ chiếu",
      value: 0,
    });
    setLichChieu(arrLichChieu);
  };

  const handleChangeSelect = () => {};

  //call api
  useEffect(() => {
    phimService
      .getDanhSachPhim()
      .then((res) => [
        setFilmSelectList(res.data),
        dispatch(setFilmList(res.data)),
      ])
      .catch((err) => console.log("err", err));
  }, []);

  // console.log("filmSelectList", filmSelectList);
  // console.log("movTheaterList", movTheaterList);
  // console.log("rapList", rapList);

  const getAllRap = (rapList) => {
    movTheaterList?.forEach((rap) => {
      let newArr = rap.cumRapChieu.map((detail) => {
        return {
          value: detail.maCumRap,
          label: detail.tenCumRap,
        };
      });
      rapList.push(...newArr);
    });
    return rapList;
  };

  const getLichChieuPhim = (value) => {
    let findLichChieu = [];
    for (let i = 0; i < movTheaterList.length; i++) {
      findLichChieu = movTheaterList[i].cumRapChieu.find(
        (cumrap) => cumrap.maCumRap == value
      );
      if (findLichChieu) break;
      else continue;
    }
    return findLichChieu.lichChieuPhim;
  };

  return (
    <div className="relative">
      <Modal
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
      >
        <iframe
          width="100%"
          height="508"
          src={source[curVid]}
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
          allowfullscreen
        ></iframe>
      </Modal>
      <Carousel afterChange={onChange} ref={carouselRef}>
        <div className="relative carousel__wrapper">
          <button
            className="transition-all opacity-0 absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2  text-white"
            onClick={showModal}
          >
            <i class="fa-solid fa-play text-5xl border-white block border-solid border-x-2 border-y-2 rounded-full w-20 h-20 text-center leading-normal bg-black"></i>
          </button>
          <button
            className="absolute -translate-y-1/2 top-1/2 left-6"
            onClick={() => handleChangeCarousel(false)}
          >
            <i class="fa-solid fa-chevron-left text-white text-5xl"></i>
          </button>
          <img
            style={contentStyle}
            src="https://s3img.vcdn.vn/123phim/2021/04/lat-mat-48h-16177782153424.png"
            class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}"
            alt=""
          />
          <button
            className="absolute -translate-y-1/2 top-1/2 right-6"
            onClick={() => handleChangeCarousel(true)}
          >
            <i class="fa-solid fa-chevron-right text-white text-5xl"></i>
          </button>
        </div>
        <div className="relative carousel__wrapper">
          <button
            className="transition-all opacity-0 absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2  text-white"
            onClick={showModal}
          >
            <i class="fa-solid fa-play text-5xl border-white block border-solid border-x-2 border-y-2 rounded-full w-20 h-20 text-center leading-normal bg-black"></i>
          </button>
          <button
            className="absolute -translate-y-1/2 top-1/2 left-6"
            onClick={() => handleChangeCarousel(false)}
          >
            <i class="fa-solid fa-chevron-left text-white text-5xl"></i>
          </button>
          <img
            style={contentStyle}
            src="https://s3img.vcdn.vn/123phim/2021/04/ban-tay-diet-quy-evil-expeller-16177781815781.png"
            class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}"
            alt=""
          />
          <button
            className="absolute -translate-y-1/2 top-1/2 right-6"
            onClick={() => handleChangeCarousel(true)}
          >
            <i class="fa-solid fa-chevron-right text-white text-5xl"></i>
          </button>
        </div>
        <div className="relative carousel__wrapper">
          <button
            className="transition-all opacity-0 absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2  text-white"
            onClick={showModal}
          >
            <i class="fa-solid fa-play text-5xl border-white block border-solid border-x-2 border-y-2 rounded-full w-20 h-20 text-center leading-normal bg-black"></i>
          </button>
          <button
            className="absolute -translate-y-1/2 top-1/2 left-6"
            onClick={() => handleChangeCarousel(false)}
          >
            <i class="fa-solid fa-chevron-left text-white text-5xl"></i>
          </button>
          <img
            style={contentStyle}
            src="https://s3img.vcdn.vn/123phim/2021/04/nguoi-nhan-ban-seobok-16177781610725.png"
            class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}"
            alt=""
          />
          <button
            className="absolute -translate-y-1/2 top-1/2 right-6"
            onClick={() => handleChangeCarousel(true)}
          >
            <i class="fa-solid fa-chevron-right text-white text-5xl"></i>
          </button>
        </div>
      </Carousel>
      <div className="choose__section flex justify-between items-center absolute z-20 left-1/2 -translate-x-1/2 -bottom-9 p-5 bg-white w-my rounded-md shadow-md">
        {/**api: https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP09 */}
        <Select
          className="my-select"
          defaultValue="Phim"
          style={{
            width: 160,
            margin: "0 4px",
          }}
          onChange={handleChangePhim}
          options={
            filmSelectList &&
            filmSelectList.map((film) => {
              return {
                label: film.tenPhim,
                value: film.maPhim,
              };
            })
          }
        />
        <Select
          className="my-select"
          defaultValue="Rạp"
          style={{
            width: 160,
            margin: "0 4px",
          }}
          onChange={handleChangeRap}
          options={filmSelectList && getAllRap(rapList)}
        />
        <Select
          className="my-select"
          defaultValue="Ngày giờ chiếu"
          style={{
            width: 160,
            fontWeight: "bold",
            margin: "0 4px",
          }}
          onChange={handleChangeSelect}
          options={filmSelectList && lichChieu}
        />
        <button className="my-btn">Mua vé ngay</button>
      </div>
    </div>
  );
}
