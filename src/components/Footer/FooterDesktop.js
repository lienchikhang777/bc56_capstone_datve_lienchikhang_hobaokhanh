import React from "react";

export default function FooterDesktop() {
  return (
    <div className="footer__wrapper">
      <div className="container py-10">
        <div className="grid grid-cols-2">
          <div className="">
            <p className="mb-2">
              Ứng dụng tiện lợi dành cho người yêu điện ảnh
            </p>
            <p>
              Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và
              đổi quà hấp dẫn.
            </p>
            <button>APP MIỄN PHÍ - TẢI VỀ NGAY</button>
            <p>TIX có hai phiên bảnIOS&Android</p>
          </div>
          <div className="grid grid-cols-2">
            <div className="text-center">
              <h3 className="text-xl font-medium mb-4">Chinh sách</h3>
              <ul className="list-none">
                <li>Term of use</li>
                <li>Private Policy</li>
                <li>Security</li>
              </ul>
            </div>
            <div className="text-center">
              <h3 className="text-xl font-medium mb-4">Tài khoản</h3>
              <ul className="list-none">
                <li>My account</li>
                <li>WatchList</li>
                <li>Collections</li>
                <li>User guides</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
